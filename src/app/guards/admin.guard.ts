import { CanActivateFn } from '@angular/router';
import { inject } from "@angular/core";
import { UserService } from "../services/user.service";

export const adminGuard: CanActivateFn = () => {
  return inject(UserService).hasAdminRole();
}
