import { Component } from '@angular/core';
import { UserService } from "../services/user.service";
import { User } from "../models/user";

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss']
})
export class Page1Component {
  constructor(private userService: UserService) {
  }

  get hasAdminRole(): boolean {
    return this.userService.hasAdminRole();
  }

  get hasManagerRole(): boolean {
    return this.userService.hasManagerRole();
  }

  get hasClientRole(): boolean {
    return this.userService.hasClientRole();
  }

  get userData(): User | null {
    return this.userService.getUserData();
  }
}
