export interface Product {
  id: number
  name: string
  desc: string
  imgUri: string
  price: number
  categoryId: number | null
}
