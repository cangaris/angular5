export interface User {
  userId: number
  firstName: string
  lastName: string
  email: string
  role: string
  password: string
  details: Details
  phoneNumber: PhoneNumber[]
  address: Address[]
}

export interface Details {
  userDetailsId: number
  taxNumber: string
  personalNumber: string
}

export interface PhoneNumber {
  phoneId: number
  prefix: string
  number: string
}

export interface Address {
  addressId: number
  city: string
  street: string
}
