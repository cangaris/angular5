import { Component } from '@angular/core';
import { UserService } from "../services/user.service";
import { switchMap } from "rxjs";
import { LoginData } from "../models/login";
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  form = this.fb.group({
    username: ['admin@webcoders.pl', [Validators.required, Validators.maxLength(200)]],
    password: ['secretPass', [Validators.required, Validators.maxLength(200)]],
    rememberMe: false,
  });

  constructor(private userService: UserService, private fb: FormBuilder) {
  }

  logout(): void {
    this.userService.logout()
      .subscribe({
        next: () => this.userService.setLoggedUser(null),
        error: error => console.error(error),
      });
  }

  login(): void {
    if (this.form.invalid) {
      return alert("Formularz niepoprawny");
    }
    const loginData = this.form.value as LoginData;
    this.userService.login(loginData)
      .pipe(switchMap(value => this.userService.whoAmI()))
      .subscribe({
        next: value => this.userService.setLoggedUser(value),
        error: error => console.error(error),
      });
  }
}
