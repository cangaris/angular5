import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { apiUrl, httpConfig } from "./common";
import { User } from "../models/user";
import { RoleEnum } from "../models/role.enum";
import { LoginData } from "../models/login";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // private loggedUser = new Subject<User | null>();
  private loggedUser = new BehaviorSubject<User | null>(null);

  constructor(private httpClient: HttpClient) {
  }

  setLoggedUser(user: User | null) {
    this.loggedUser.next(user);
  }

  getLoggedUser(): Observable<User | null> {
    return this.loggedUser.asObservable();
  }

  login(loginData: LoginData): Observable<void> {
    const body = new FormData();
    body.set('username', loginData.username);
    body.set('password', loginData.password);
    body.set('rememberMe', loginData.rememberMe.toString());
    return this.httpClient.post<void>(`${apiUrl}/login`, body, httpConfig);
  }

  logout(): Observable<void> {
    return this.httpClient.post<void>(`${apiUrl}/logout`, null, httpConfig);
  }

  whoAmI(): Observable<User> {
    return this.httpClient.get<User>(`${apiUrl}/user/who-am-i`, httpConfig);
  }

  hasAdminRole(): boolean {
    return this.loggedUser.getValue()?.role === RoleEnum.ADMIN;
  }

  hasClientRole(): boolean {
    return this.loggedUser.getValue()?.role === RoleEnum.CLIENT;
  }

  hasManagerRole(): boolean {
    return this.loggedUser.getValue()?.role === RoleEnum.MANAGER;
  }

  getUserData(): User | null {
    return this.loggedUser.getValue();
  }
}
