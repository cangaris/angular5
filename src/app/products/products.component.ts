import { Component, OnInit } from '@angular/core';
import { Product } from "../models/product";
import { ProductService } from "../services/product.service";
import { UserService } from "../services/user.service";
import { User } from "../models/user";
import { filter, Observable, switchMap, tap } from "rxjs";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
  productLoading = false;
  selectedId?: number;

  constructor(private productService: ProductService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.loggedUser()
      .pipe(
        filter(value => !!value),
        tap(value => this.productLoading = true),
        switchMap(value => this.productService.getProducts()),
      )
      .subscribe({
        next: (value) => {
          this.productLoading = false;
          this.products = value;
        },
        error: (err) => {
          this.productLoading = false;
          console.error(err);
        },
      });
  }

  loggedUser(): Observable<User | null> {
    return this.userService.getLoggedUser();
  }

  catchSelectedProductId(productId: number) {
    this.selectedId = productId;
  }
}
