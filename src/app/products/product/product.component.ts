import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from "../../models/product";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  @Input() product!: Product;
  @Output() selectedProduct = new EventEmitter<number>();

  selectProduct(productId: number) {
    this.selectedProduct.emit(productId);
  }
}
