import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { HttpClientModule } from "@angular/common/http";
import { LoginComponent } from './login/login.component';
import { Route, RouterModule } from "@angular/router";
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { adminGuard } from "./guards/admin.guard";
import { ProductComponent } from './products/product/product.component';
import { ReactiveFormsModule } from "@angular/forms";

const routes: Route[] = [
  { path: 'page1', component: Page1Component, canActivate: [] },
  { path: 'page2', component: Page2Component, canActivate: [adminGuard] },
  { path: '**', redirectTo: '/page1' }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    Page1Component,
    Page2Component,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
